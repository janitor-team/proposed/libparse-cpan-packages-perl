libparse-cpan-packages-perl (2.40-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:36:08 +0100

libparse-cpan-packages-perl (2.40-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 20:34:53 +0100

libparse-cpan-packages-perl (2.40-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 2.40
  * Update (build-)dependencies: switch from Moose to Moo
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able
  * Refresh patches (offset)

 -- Florian Schlichting <fsfs@debian.org>  Fri, 13 Nov 2015 22:43:40 +0100

libparse-cpan-packages-perl (2.38-1) unstable; urgency=low

  [ Nuno Carvalho ]
  * New upstream release 2.37

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release 2.38.
  * Update years of packaging copyright.
  * Set Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Feb 2013 18:16:26 +0100

libparse-cpan-packages-perl (2.36-1) unstable; urgency=low

  * Team upload
  * d/control: Remove libtest-pod* from B-D-I
  * d/control: Update standards version
  * d/copyright: Update copyright format
  * New upstream release

 -- Nuno Carvalho <smash@cpan.org>  Wed, 11 Jul 2012 22:46:04 +0100

libparse-cpan-packages-perl (2.35-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.

  [ Jonathan Yu ]
  * New upstream release 2.33
  * Use new 3.0 (quilt) source format
  * Use new short debhelper rules format
  * Refresh copyright information
  * Refresh patches and headers
  * Rewrite control description
  * Standards-Version 3.9.1 (no changes)
  * Add myself to Uploaders and Copyright

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream releases 2.34, 2.35.
  * Refresh patch.
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.2 (no changes).
  * Add new build and runtime dependencies.
  * Refresh patch (offset).

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 Sep 2011 16:08:48 +0200

libparse-cpan-packages-perl (2.31-1) unstable; urgency=low

  [ Brian Cassidy ]
  * New upstream release
  * debian/control:
    + Standards-Version to 3.8.1
    + Added myself to Uploaders

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders (Closes: #523222)

 -- Brian Cassidy <brian.cassidy@gmail.com>  Fri, 24 Apr 2009 09:44:03 -0300

libparse-cpan-packages-perl (2.30-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * New upstream release.
    + Update copyright years in debian/copyright.
  * debian/control: Update dependencies.
  * Add missing whatis entry for Parse::CPAN::Distribution.
    + Add quilt framework and README.source to debian/*.

  [ gregor herrmann ]
  * debian/control:
    - add (build) dependency on perl-modules (>= 5.10) | libcompress-zlib-perl
    - mention module name in long description

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 23 Jan 2009 20:56:11 +0100

libparse-cpan-packages-perl (2.29-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
    + Update copyright years in debian/copyright
  * Add myself to Uploaders.

  [ gregor herrmann ]
  * debian/control: (build) depend on "perl-modules (>= 5.10) |
    libversion-perl" instead of libversion-perl alone.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 22 Aug 2008 18:00:36 +0200

libparse-cpan-packages-perl (2.28-1) unstable; urgency=low

  [ Frank Lichtenheld ]
  * Add a missing comma in Uploaders field

  [ gregor herrmann ]
  * New upstream release.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * debian/control: change my email address.
  * Switch debian/copyright to new format.
  * Set Standards-Version to 3.8.0 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2008 17:20:44 -0300

libparse-cpan-packages-perl (2.27-2) unstable; urgency=low

  [ Joachim Breitner ]
  * Removed myself from uploaders.

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #468006).
  * debian/copyright: use author-agnostic download URL.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 09 Mar 2008 23:35:39 +0100

libparse-cpan-packages-perl (2.27-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/control:
    - set Standards-Version to 3.7.3 (no changes required)
    - wrap long lines
  * debian/watch: use dist-based URL.
  * debian/rules:
    - remove compiler flags (this is an arch:all package)
    - move tests to build target
    - move dh_clean before make distclean
    - remove unused dh_* calls
    - don't install README (just a copy of pod/manpage)

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 25 Dec 2007 23:55:23 +0100

libparse-cpan-packages-perl (2.26-2) unstable; urgency=low

  * Added dependency on libversion-perl (Closes: #432469)

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 09 Jul 2007 23:49:23 -0500

libparse-cpan-packages-perl (2.26-1) unstable; urgency=low

  * New upstream release.
  * Set Debhelper Compatibility Level to 5.
  * Set Standards-Version to 3.7.2 (no changes).
  * Added libversion-perl to Build-Depends-Indep.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 17 Jun 2006 18:52:57 +0200

libparse-cpan-packages-perl (2.25-1) unstable; urgency=low

  * New upstream release. (Closes: #329652)
  * Add myself to Uploaders.
  * Don't ignore the return code of 'make distclean'.
  * Upgrade to Standards-Version 3.6.2. No changes needed.

 -- Niko Tyni <ntyni@iki.fi>  Fri,  6 Jan 2006 19:31:57 +0200

libparse-cpan-packages-perl (2.21-1) UNRELEASED; urgency=low

  * Florian Ragwitz:
    * New upstream release
    * debian/control: added libio-zlib-perl to {Build-,}Depends.

 -- Florian Ragwitz <florian@mookooh.org>  Sun, 29 Aug 2004 19:02:27 +0200

libparse-cpan-packages-perl (2.20-1) unstable; urgency=low

  * Initial Release.

 -- Florian Ragwitz <florian@mookooh.org>  Thu, 13 May 2004 22:15:08 +0200
